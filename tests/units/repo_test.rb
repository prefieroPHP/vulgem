#require 'rubygems'
require 'test/unit'
require 'models/repo'

REPO_PATH = './data/'

class RepoTest < Test::Unit::TestCase
  def test_essential
     assert true
  end
  
  def test_instantiate
     assert repo = (Repo.new 'test2')
     assert_equal repo.class.to_s, 'Repo'
  end
  
  def test_new
     repo = Repo.new 'test2'
     assert repo.save
  end
  
  def test_new_fail
     repo = Repo.new 'test2:'
     assert_equal repo.save, false
  end


end
