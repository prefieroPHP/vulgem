#require 'rubygems'
require 'test/unit'
require 'models/repo'
require 'models/gem'
require 'models/version'

class VersionTest < Test::Unit::TestCase

  def setup
    @repo = Repo.new 'test2'
    @repo.save
    @gem = Gem.new @repo, 'test2'
    @gem.save
  end
  
  def test_essential
     assert true
  end
  
  def test_instantiate
    setup
    assert v = (Version.new @repo, @gem, '1.0.1')
    assert_equal v.class.to_s, 'Version'
  end

  def test_new
    setup
    v = Version.new @repo, @gem, '1.0.1'
    assert v.save
  end
    
  def test_new_fail
    setup
    v = Version.new @repo, @gem, 'test2'
    assert_equal v.save, false
  end
  
end
