#require 'rubygems'
require 'test/unit'
require 'models/repo'
require 'models/gem'

class GemTest < Test::Unit::TestCase

  def setup
    @repo = (Repo.new 'test2')
    @repo.save
  end
  
  def test_essential
     assert true
  end
  
  def test_instantiate
    setup
    assert gem = (Gem.new @repo, 'test2')
    assert_equal gem.class.to_s, 'Gem'
  end
  
  def test_new
    setup
    gem = Gem.new @repo, 'test2'
    assert gem.save
  end

  def test_new_fail
    setup
    gem = Gem.new @repo, 'test2:'
    assert_equal gem.save, false
  end
  
end
