class Repo
  attr_reader :name

  def initialize name
    @name = name
  end
  
  def check
     !(/^[a-zA-Z0-9._-]*\Z/ =~ @name).nil?
  end

  def path
     './data/' + @name
  end
  
  def save
    return false if !check
    begin
	  Dir::mkdir(path) unless File.directory?(path)
    rescue
	  return false
    end
    true
  end
  
end
