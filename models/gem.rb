class Gem
  attr_reader :repo, :name

  def initialize repo, name
    @repo = repo
    @name = name
  end
  
  def check
     !(/^[a-zA-Z0-9._-]*\Z/ =~ @name).nil?
  end
  
  def path
    repo.path + '/' + @name
  end
  
  def save
    return false if !self.check
    begin
      Dir::mkdir(path) unless File.directory?(path)
    rescue
      return false
    end
    true
  end
end
