class Version
  attr_reader :repo,:gem, :version

  def initialize repo, gem, version
    @version = version
    @repo = repo
    @gem = gem
  end
  
  def check
     /[0-9].[0-9].[0-9]/ =~ @version
  end

  def path
    @gem.path + '/' + @version.to_s
  end
    
  def save
    return false if !self.check
    begin
      Dir::mkdir(path) unless File.directory?(path)
    rescue
      return false
    end
    true
  end
end
